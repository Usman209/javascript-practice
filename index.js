// destructuring of Object

// const note = {
//   id: 1,
//   title: "my first note",
//   date: "01/01/1970",
//   author: {
//     firstName: "hello",
//     lastName: "world",
//   },
// };

// Destructor of nested properties

// const {
//   id,
//   title,
//   date,
//   author:{firstName,lastName}
// } = note // here we getting values of note e.g note.id, note.title

// console.log(`${firstName}`) // run work

// console.log(author) // error 1 : ReferenceError: author is not defined

// error 1 sol:

// const {
//   id,
//   title,
//   date,
//   author,
//   author: { firstName, lastName },
// } = note;

// console.log(author);

// we can also get properties of string directly
const { length } = "this is a string";
console.log(length);

//  array 

// const dates = ['1970', '12', '01']


// const [year, month, day ] = dates

// console.log(year)




// 

// nested array 

const nestedArray =[1,2,3,[4,5],6]

const [one,two,three,[four,five],six]=nestedArray


console.log(one,five)


// Object destructuring and array destructuring can be combined in a single destructuring assignment.

const note1 = {
  title: 'My first note',
  author: {
    firstName: 'Sherlock',
    lastName: 'Holmes',
  },
  tags: ['personal', 'writing', 'investigations'],
}

// even getting values of note1 object can insert data..same like property 
const {
  title,
  date = new Date(),

  author:{firstName},
  tags:[a,b]
} =note1

console.log(date)


// Spread with Arrays

const tools = ['hammer', 'screwdriver']
const otherTools = ['wrench', 'saw']

// Originally you would use concat() to concatenate the two arrays:


const allTools =tools.concat(otherTools)
console.log(allTools)

// Now you can also use spread to unpack the arrays into a new array:


const Tool =[...tools ,...otherTools]  // this did same job like above concat() method

console.log(Tool)


const users = [
  { id: 1, name: 'Ben' },
  { id: 2, name: 'Leslie' },
]


// want to add new user 


const newUser = {
  id :3,
  name:'user 3'
}

// users.push(newUser)   // this is one way . also simple this thing can done by following way 

console.log(users)


const updateUser= [...users , newUser]

console.log(updateUser)



const originalArray = ['one', 'two', 'three']

 const secondArray =originalArray

 secondArray.pop()
 console.log(originalArray)     // in javascript passes references  like here here [ const secondArray =originalArray]  so remove item effect original variable 


 // Spread allows you to make a shallow copy of an array or object,  example below 

 const originalArray1 = ['one', 'two', 'three']

 // Use spread to make a shallow copy
 const secondArray1 = [...originalArray1]
 
 // Remove the last item of the second Array
 secondArray.pop()
 
 console.log(originalArray1)
 console.log('sec :',secondArray1)


//  Spread can also be used to convert a set, or any other iterable to an Array.


const set = new Set()

set.add('octopus')
set.add('starfish')
set.add('whale')

const seaCreatures = [...set]

console.log(seaCreatures)
 
// This can also be useful for creating an array from a string:


const string = 'hello'

const stringArray = [...string]

console.log(stringArray)


//  Spread with Objects

// Create an object and a copied object with spread
const originalObject = { enabled: true, darkMode: false }
const secondObject = { ...originalObject }

console.log(secondObject)

// Adding or modifying properties on an existing object in an immutable fashion is simplified with spread. In this example, the isLoggedIn property is added to the user object:



const user2 = {
  id:4,
  name:'good'

}


updateUser2 ={...user2 ,isLogin:true}

console.log(updateUser2)


// nested object case 


const user4 = {
  id: 3,
  name: 'Ron',
  organization: {
    name: 'Parks & Recreation',
    city: 'Pawnee',
  },
}


const updatedUser4 = { ...user4, organization: { position: 'Director' } } //   If you tried to add a new item to organization, it would overwrite the existing fields:



console.log(updatedUser4)


// so solution of above 

const updatedUser = {
  ...user4,
  organization: {
    ...user4.organization,
    position: 'Director',
  },
}

console.log(updatedUser)



// Spread with Function Calls


// Create a function to multiply three items
function multiply(a, b, c) {
  return a * b * c
}
// multiply(1,2,3)

console.log(multiply(1,2,3))


// same with Spread with Function Calls

const number =[1,2,4]
console.log(multiply(...number))

// same thing can be done with apply 

console.log(multiply.apply(null, [1, 2, 5]))

// Rest Parameters



// In the function restTest for example, if we wanted args to be an array composed of an indefinite number of arguments, we could have the following:

function restTest(...args) {
  console.log(args)
}

restTest(1, 2, 3, 4, 5, 6,7) // we can pass as many as we want


// Rest syntax can be used as the only parameter or as the last parameter in the list. If used as the only parameter, it will gather all arguments, but if it's at the end of a list, it will gather every argument that is remaining, as seen in this example:
function restTest(one, two, ...args) {
  console.log(one)
  console.log(two)
  console.log(args)
}

restTest(1, 2, 3, 4, 5, 6)


function testArguments() {
  console.log(arguments)
}

testArguments('how', 'many', 'arguments')


// with arrow function give error :

// const testArguments1 = () => {
//   console.log(arguments)
// }

// testArguments1('how', 'many', 'arguments') // throw error 

//  can be used when destructuring arrays as well:
const [firstTool, ...rest] = ['hammer', 'screwdriver', 'wrench']

console.log(firstTool)
console.log(rest)


//  can also be used when destructuring objects:


const { isLoggedIn, ...rest1 } = { id: 1, name: 'Ben', isLoggedIn: true }

console.log(isLoggedIn)
console.log(rest1)